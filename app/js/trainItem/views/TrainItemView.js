var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	stickit             = require('backbone.stickit'),
	SubmitStatusView	= require('../../submitStatus/views/SubmitStatusView');

module.exports = TrainItemView = Mm.View.extend({
	tagName: 'li',

	className: 'list-group-item',

	template: require("../templates/trainLine.hbs"),

	ui: {
		progressBarGood: '.progress-bar.bg-success',
		progressBarSlow: '.progress-bar.bg-warning',
		progressBarFault: '.progress-bar.bg-danger'
	},

	regions: {
		submitStatus: '.submit-status'
	},

	onRender () {

		this.model.on('change', () => {
			this.updateProgressLine();
		});

		this.showSubmitStatusView();
	},

	showSubmitStatusView () {
		this.showChildView('submitStatus', new SubmitStatusView({
			model: this.model
		}));
	},

	updateProgressLine () {
		var rep = this.model.pick('faultReport', 'goodReport', 'slowReport');
		var highestRep = Object.keys(rep).reduce(function(a, b){ return rep[a] > rep[b] ? a : b });

		this.$el.find('.progress .progress-bar .progress-status').hide();
		this.$el.find('.progress .progress-bar.' + highestRep + ' .progress-status').removeAttr('style');

		if (_.size(this.model.get('goodReport')) > 0) {
			this.getUI('progressBarGood').width((_.size(this.model.get('goodReport')) / this.model.get('totalReport')) * 100 + '%');
		} else {
			this.getUI('progressBarGood').width(0);
		}

		if (_.size(this.model.get('slowReport')) > 0) {
			this.getUI('progressBarSlow').width((_.size(this.model.get('slowReport')) / this.model.get('totalReport')) * 100 + '%');
		} else {
			this.getUI('progressBarSlow').width(0);
		}

		if (_.size(this.model.get('faultReport')) > 0) {
			this.getUI('progressBarFault').width((_.size(this.model.get('faultReport')) / this.model.get('totalReport')) * 100 + '%');
		} else {
			this.getUI('progressBarFault').width(0);
		}

		if (this.model.get('totalReport') == 0) {
			this.getUI('progressBarGood').width('100%');
		}

		var goodMessage = '';
		var slowMessage = '';
		var faultMessage = '';
		var defaultMessage = [
			`No news is good news`,
			`Hopefully..`,
			`Not green? Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`,
			`Wrong ah? Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`,
			`Salah? Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`,
			`No complaints yet`
		]

		if (this.model.get('totalReport') == 0) {
			goodMessage = _.sample(defaultMessage);
		}

		this.getUI('progressBarGood').popover('dispose').popover({
			content: goodMessage ? goodMessage : _.size(this.model.get('goodReport')) + " report(s)",
			placement: 'top',
			trigger: 'hover',
			html: true
		});

		if (_.size(this.model.get('slowReport')) > 0) {
			slowMessage = _.size(this.model.get('slowReport')) + " report(s)<br>";

			var slowReport = this.model.get('slowReport');
			var moreSlowReports = '';

			if (_.size(slowReport) > 5) {
				slowReport = _.sample(this.model.get('slowReport'), 5);
				moreSlowReports = '<br>' + _.size(slowReport) - 5 + ' reports more...';
			}

			var slowReportStations = '<ul class="reportList">' +  _.compact(_.map(slowReport, function(rep) {
				if (rep.from_value && rep.to_value) {
					return '<li>' + rep.from_value + ' to ' + rep.to_value + '</li>';
				}
			})).join('') + '</ul>';

			slowMessage += slowReportStations + moreSlowReports;
		}

		this.getUI('progressBarSlow').popover('dispose').popover({
			content: slowMessage,
			placement: 'top',
			trigger: 'hover',
			html: true
		});

		if (_.size(this.model.get('faultReport')) > 0) {
			faultMessage = _.size(this.model.get('faultReport')) + " report(s)<br>";

			var faultReport = this.model.get('faultReport');
			var moreFaultReports = '';

			if (_.size(faultReport) > 5) {
				faultReport = _.sample(this.model.get('faultReport'), 5);
				moreFaultReports = '<br>' + _.size(faultReport) - 5 + ' reports more...';
			}

			var faultReportStations = '<ul class="reportList">' +  _.compact(_.map(faultReport, function(rep) {
				if (rep.from_value && rep.to_value) {
					return '<li>' + rep.from_value + ' to ' + rep.to_value + '</li>';
				}
			})).join('') + '</ul>';

			faultMessage += faultReportStations + moreFaultReports;
		}

		this.getUI('progressBarFault').popover('dispose').popover({
			content: faultMessage,
			placement: 'top',
			trigger: 'hover',
			html: true
		});
	}
});