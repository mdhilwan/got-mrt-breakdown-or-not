var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),

	Frb 				= require('../../frb/Frb'),

	TrainListView 		= require('../../trainList/views/TrainListView'),
	ResolutionView 		= require('../../resolution/views/ResolutionView'),
	AboutMeModal		= require('../../aboutMe/views/AboutMeModal'),
	RootStatusView		= require('../../rootStatus/views/RootStatusView'),
	ShareModal			= require('../../shareModal/views/ShareModal'),
	SubmitStatusModal	= require('../../submitStatus/views/SubmitStatusModal');

var firebaseRad 		= Radio.channel('firebaseRad');
var statusRad 			= Radio.channel('statusRad');

var rootPath			= 'trainStatus';
var currReso 			= '0.5'; // hour

require('block-ui');

module.exports = RootView = Mm.View.extend({
	template: require("../templates/root.hbs"),

	className: "container",

	regions: {
		trainList		: "#train-list",
		status			: "#status",
		resolution		: "#resolution",
		modalContainer	: "#modal-container"
	},

	initialize () {
		this.listenTo(firebaseRad, 	'trainDetail:data:loaded', this.showTrainDetail);
		this.listenTo(firebaseRad, 	'data:stored', this.shareReport);
		this.listenTo(statusRad, 	'submitReport', this.submitReport);
		this.listenTo(statusRad, 	'aboutView', this.viewAboutMe);
	},

	onRender () {
		this.frb = new Frb();

		Bb.history.start();

		this.TRAIN_COLLECTION = new Bb.Collection([
			{ 
				id: "ewl",
				lineName: "East West Line",
				className: "ew-line"
			},
			{ 
				id: "nsl",
				lineName: "North South Line",
				className: "ns-line"
			},
			{ 
				id: "nel",
				lineName: "North East Line",
				className: "ne-line"
			},
			{ 
				id: "ccl",
				lineName: "Circle Line",
				className: "cc-line"
			},
			{ 
				id: "dtl",
				lineName: "Downtown Line",
				className: "dt-line"
			},
		]);

		this.RESOLUTION_MODEL 	= new Backbone.Model({ reso: currReso });

		this.TRAIN_LIST_VIEW 	= new TrainListView({ collection: this.TRAIN_COLLECTION });
		this.ROOT_STATUS_VIEW 	= new RootStatusView({ collection: this.TRAIN_COLLECTION });
		this.RESOLUTION_VIEW 	= new ResolutionView({ model: this.RESOLUTION_MODEL });

		this.showChildView('trainList', this.TRAIN_LIST_VIEW);
		this.showChildView('status', this.ROOT_STATUS_VIEW);
		this.showChildView('resolution', this.RESOLUTION_VIEW);
		this.reloadReport();

		this.RESOLUTION_MODEL.on('change', () => {
			this.reloadReport();
		})
	},

	showTrainDetail(val) {
		// if (_.isNull(val.db)) return false;

		this.TRAIN_COLLECTION.each((line) => {
			
			var lineReport 	= _.where(val.db, { trainLine: line.get('id') });

			var goodReport 	= _.where(lineReport, { reportType: 'good'	});
			var slowReport 	= _.where(lineReport, { reportType: 'slow'	});
			var faultReport = _.where(lineReport, { reportType: 'fault' });
			var totalReport = _.size(goodReport) + _.size(slowReport) + _.size(faultReport);

			line.set({
				// report: report,
				goodReport: goodReport,
				slowReport: slowReport,
				faultReport: faultReport,
				totalReport: totalReport
			}); 
		});

		this.frb.dataDeleteOld();
	},

	submitReport (status) {
		var submitStatusModal = new SubmitStatusModal({
			model: new Backbone.Model(status)
		});

		this.showChildView('modalContainer', submitStatusModal)
	},

	viewAboutMe () {
		var aboutMe = new AboutMeModal();

		this.showChildView('modalContainer', aboutMe)
	},

	shareReport() {
		var shareModal = new ShareModal();

		this.showChildView('modalContainer', shareModal);
		this.reloadReport();
	},

	reloadReport () {
		firebaseRad.trigger('data:getFromPath', {
			path: rootPath,
			reso: this.RESOLUTION_MODEL.get('reso'),
			callback: 'trainDetail:data:loaded'
		});	
	},

	block (msg) {
		console.log(msg);

		$.blockUI({
            message: msg,
            css: {
                border: 'none',
                padding: '15px',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                opacity: .5,
                color: '#fff'
            }
        });
	},
});