var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	stickit             = require('backbone.stickit');

var statusRad 			= Radio.channel('statusRad');

module.exports = AboutMeModal = Mm.View.extend({

	template: require("../templates/aboutMe.hbs"),	

	ui: {
		aboutModal: 		"#aboutModal"
	},

	onRender () {
		this.getUI('aboutModal').modal();
	}

});