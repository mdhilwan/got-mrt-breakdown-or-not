var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	TrainItemView 		= require('../../trainItem/views/TrainItemView');

module.exports = TrainListView = Mm.CollectionView.extend({
	tagName: 'ul',

	className: 'list-group',

	childView: TrainItemView
});