var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio');

var statusRad 			= Radio.channel('statusRad');
var firebaseRad 		= Radio.channel('firebaseRad');

require('ion-rangeslider')

var rootPath			= 'trainStatus';

var lines 				= {
	ew_line: [
		"Tuas Link", "Tuas West Road",
		"Tuas Crescent", "Gul Circle",
		"Joo Koon", "Pioneer",
		"Boon Lay", "Lakeside",
		"Chinese Garden", "Jurong East",
		"Clementi", "Dover", 
		"Buona Vista", "Commonwealth", 
		"Queenstown", "Redhill", 
		"Tiong Bahru", "Outram Park", 
		"Tanjong Pagar", "Raffles Place", 
		"City Hall", "Bugis", 
		"Lavender", "Kallang", 
		"Aljunied", "Paya Lebar", 
		"Eunos", "Kembangan", 
		"Bedok", "Tanah Merah", 
		"Simei", "Tampines", 
		"Pasir Ris", "Expo", 
		"Changi Airport"
	],
	ns_line: [
		"Jurong East", "Bukit Batok", 
		"Bukit Gombak", "Choa Chu Kang", 
		"Yew Tee", "Kranji", 
		"Marsiling", "Woodlands", 
		"Admiralty", "Sembawang", 
		"Canberra", "Yishun", 
		"Khatib", "Yio Chu Kang", 
		"Ang Mo Kio", "Bishan", 
		"Circle Line", "Braddell", 
		"Toa Payoh", "Novena", 
		"Newton", "Orchard", 
		"Somerset", "Dhoby Ghaut", 
		"City Hall", "Raffles Place", 
		"Marina Bay", "Marina South Pier"
	],
	ne_line: [
		"HarbourFront", "Outram Park", 
		"Chinatown", "Clarke Quay", 
		"Dhoby Ghaut", "Little India", 
		"Farrer Park", "Boon Keng", 
		"Potong Pasir", "Woodleigh", 
		"Serangoon", "Kovan", 
		"Hougang", "Buangkok", 
		"Sengkang", "Punggol", 
		"Punggol Coast"
	],
	cc_line: [
		"Dhoby Ghaut", "Bras Basah", 
		"Esplanade", "Promenade", 
		"Downtown", "Bayfront", 
		"Marina Bay", "Nicoll Highway", 
		"Stadium", "Mountbatten", 
		"Dakota", "Paya Lebar", 
		"East West", "MacPherson", 
		"Tai Seng", "Bartley", 
		"Serangoon", "Lorong Chuan", 
		"Bishan", "Marymount", 
		"Caldecott", "Bukit Brown", 
		"Botanic Gardens", "Farrer Road", 
		"Holland Village", "one-north", 
		"Kent Ridge", "Haw Par Villa", 
		"Pasir Panjang", "Labrador Park", 
		"Telok Blangah", "HarbourFront", 
		"Bayfront", "Marina Bay", "North South"
	],
	dt_line: [
		"Bukit Panjang", "Cashew", 
		"Hillview", "Beauty World", 
		"King Albert Park", "Sixth Avenue", 
		"Tan Kah Kee", "Botanic Gardens", 
		"Stevens", "Newton", 
		"Little India", "Rochor", 
		"Bugis", "Promenade", 
		"Bayfront", "Downtown", 
		"Telok Ayer", "Chinatown", 
		"Fort Canning", "Bencoolen", 
		"Jalan Besar", "Bendemeer", 
		"Geylang Bahru", "Mattar", 
		"MacPherson", "Ubi", 
		"Kaki Bukit", "Bedok North", 
		"Bedok Reservoir", "Tampines West", 
		"Tampines", "Tampines East", 
		"Upper Changi", "Expo" 
	]
}

module.exports = SubmitStatusModal = Mm.View.extend({
	
	template: require("../templates/submitStatusModal.hbs"),

	ui: {
		submitModal: 			"#submitModal",
		submitStatusBtn: 		"#submitStatusBtn",
		stationRange: 			"#station_range",
		stationRangeWrapper: 	".station_range_wrapper",
		toggleStationRangeBtn: 	"#toggle_station_range"
	},

	events: {
		"click @ui.submitStatusBtn" 		: "getMyIp",
		"click @ui.toggleStationRangeBtn" 	: "toggleStationRange"
	},

	initialize () {
		this.listenTo(firebaseRad, 	'validated:ip:success', this.onSubmitStatus);
		this.listenTo(firebaseRad, 	'validated:ip:fail', this.errorMessage);
	},

	onRender () {
		this.getUI('submitModal').modal();

		this.selectedTrainLine = lines[this.model.get('className').replace('-', '_')];
		var from = Math.floor(this.selectedTrainLine.length / 3) + _.random(0, 5); 
		var to = from + _.random(2, 5);

		this.getUI('stationRange').ionRangeSlider({
			type: "double",
			decorate_both: false,
			values_separator: " to ",
			from: from,
    		to: to,
			values: this.selectedTrainLine,
			onChange: (data) => {
		        this.setStationRange(data);
		    }
		});
	},

	getMyIp () {
		this.getUI('submitStatusBtn').html('Reporting <i class="fa fa-refresh fa-spin fa-fw"></i>')
		$.get("http://ipinfo.io", (response) => {
			this.validateIp(response.ip);
		}, "jsonp");
	},

	validateIp (myIp) {
		firebaseRad.trigger('validate:ip', {
			ip: myIp,
		})
	},

	toggleStationRange () {
		if (this.getUI('toggleStationRangeBtn').hasClass('active')) {
			this.getUI('toggleStationRangeBtn').find('.fa-square').show()
			this.getUI('toggleStationRangeBtn').find('.fa-check-square').hide()
		} else {
			this.getUI('toggleStationRangeBtn').find('.fa-square').hide()
			this.getUI('toggleStationRangeBtn').find('.fa-check-square').show()
		}
		this.getUI('stationRangeWrapper').toggle();

		var slider = this.getUI("stationRange").data("ionRangeSlider");
		this.setStationRange(slider.result);
	},

	setStationRange (data) {
		if (_.isNull(data.from_value)) {
			data.from_value 	= this.selectedTrainLine[data.from];
			data.to_value 		= this.selectedTrainLine[data.to];
			data.from_pretty 	= data.from;
			data.to_pretty 		= data.to;
		}

		var stationRange = data.to - data.from;
		console.log(data)

		if (stationRange > 5) {
			this.$el.find('.station_range_status').html("Wah so many stations? Jialat 😨");
		} else {
			this.$el.find('.station_range_status').html("");
		}

		this.model.set('from_value', data.from_value);
		this.model.set('from_pretty', data.from_pretty);
		this.model.set('to_value', data.to_value);
		this.model.set('to_pretty', data.to_pretty);
	},

	onSubmitStatus (myIp) {
		var hash 		= myIp.hash || '';

		var valueEntry = {
			ip: myIp.ip,
			trainLine: this.model.get('id'),
			reportType: this.model.get('submitReport').toLowerCase()
		}

		if (this.model.has('from_value') && this.model.has('from_pretty') && this.model.has('to_value') && this.model.has('to_pretty')) {

			valueEntry.from_value 	= this.model.get('from_value');
			valueEntry.from_pretty 	= this.model.get('from_pretty');
			valueEntry.to_value 	= this.model.get('to_value');
			valueEntry.to_pretty 	= this.model.get('to_pretty');

		}

		this.getUI('submitModal').modal('toggle');

		firebaseRad.trigger('data:setToPath', {
			dir 	: rootPath + '/',
			value 	: valueEntry,
			hash 	: hash,
			callback: "data:stored"
		});
	},

	errorMessage (lastPost) {
		this.getUI('submitStatusBtn').html('Please wait ' + (5 - lastPost) + ' min');
		alert('Your last post was ' + lastPost + ' minutes ago. Please wait another ' + (5 - lastPost) + ' minutes before submitting another report')
	}

});