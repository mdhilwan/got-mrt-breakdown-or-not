var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio');

var statusRad 			= Radio.channel('statusRad');

module.exports = SubmitStatusView = Mm.View.extend({
	
	template: require("../templates/submitStatus.hbs"),

	replaceElement: true,
	
	ui: {
		btnSubmitGood: '.submit-good',
		btnSubmitSlow: '.submit-slow',
		btnSubmitFault: '.submit-fault'
	},

	events: {
		'click @ui.btnSubmitGood' : 'btnSubmitGood',
		'click @ui.btnSubmitSlow' : 'btnSubmitSlow',
		'click @ui.btnSubmitFault' : 'btnSubmitFault'
	},

	btnSubmitGood () {
		var status = this.model.toJSON();
			status.submitReport = "Good";

		statusRad.trigger('submitReport', status);
	},

	btnSubmitSlow () {
		var status = this.model.toJSON();
			status.submitReport = "Slow";

		statusRad.trigger('submitReport', status);
	},

	btnSubmitFault () {
		var status = this.model.toJSON();
			status.submitReport = "Fault";

		statusRad.trigger('submitReport', status);
	}

});