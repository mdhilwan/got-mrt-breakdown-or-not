var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	stickit             = require('backbone.stickit');

var statusRad 			= Radio.channel('statusRad');

module.exports = ShareModal = Mm.View.extend({

	template: require("../templates/shareModal.hbs"),	

	ui: {
		aboutModal: 	"#aboutModal",
		shareFb: 		".share-fb",
		shareWa: 		".share-wa"
	},

	events: {
		"click @ui.shareFb" : "onShareFb",
		"click @ui.shareWa" : "onShareWa",
	},

	onRender () {
		this.getUI('aboutModal').modal();
	},

	onShareFb () {
		
	},

	onShareWa () {
		
	},


});