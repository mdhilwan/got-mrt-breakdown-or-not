var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio'),
	stickit             = require('backbone.stickit');

var statusRad 			= Radio.channel('statusRad');

module.exports = ResolutionView = Mm.View.extend({

	template: require("../templates/resolution.hbs"),

	ui: {
		resoBtn : ".reso-btn",
		aboutBtn : ".btn-about"
	},

	events: {
		"change @ui.resoBtn" : "onResoChange",
		"click @ui.aboutBtn" : "onAboutBtn"
	},

	onResoChange (e) {
		this.model.set('reso', e.currentTarget.value);
	},

	onAboutBtn () {
		statusRad.trigger('aboutView')
	}

});