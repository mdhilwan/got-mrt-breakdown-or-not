var Bb 					= require('backbone'),
	Mm 					= require('backbone.marionette'),
	Radio 				= require('backbone.radio');

var statusRad 			= Radio.channel('statusRad');

var loading		 		= `Wait ah...<br>🚇 <i class="fa fa-refresh fa-spin fa-fw"></i>
`;

module.exports = RootStatusView = Mm.View.extend({
	template: require("../templates/rootStatus.hbs"),

	className: "lead",

	onRender () {
		var statusTimer;

		this.collection.on('change', () => {
			if (statusTimer) clearTimeout(statusTimer);

			this.loadingStatus();

			statusTimer = _.delay(() => {
				var goodReportsLength = _.flatten(this.collection.pluck('goodReport')).length;
				var slowReportsLength = _.flatten(this.collection.pluck('slowReport')).length;
				var faultReportsLength = _.flatten(this.collection.pluck('faultReport')).length;
				var totalReports = goodReportsLength + slowReportsLength + faultReportsLength
				
				this.updateStatus({good: goodReportsLength, slow: slowReportsLength, fault: faultReportsLength, total: totalReports});
			}, 500)
		})

		this.loadingStatus();
	},

	loadingStatus() {
		this.$el.html(loading);
	},

	updateStatus(reports) {
		if (reports.total == 0) {
			var zeroReport = [
				`Wah zero reports?... It seems quiet... Too quiet 🤔 Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`,
				`No news is... Good news? Hopefully la 🤔 Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`,
				`All Green!... Because no one complain yet 😅 Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`,
				`Green!... No ah? 😅 Not green ah? Report! Click the <i class="fa fa-fw fa-exclamation-circle" aria-hidden="true"></i>`
			]
			this.$el.html(_.sample(zeroReport));
		} else if (reports.good / reports.total == 1 && reports.total == 1) {
			this.$el.html(reports.good + ` out of ` + reports.total + `! 100%! Only ` + reports.total + ` report! But must still celebrate la! 🍺`)
		} else if (reports.good / reports.total == 1 && reports.total > 3) {
			this.$el.html(reports.good + ` out of ` + reports.total + ` reports! 100% green! Buy toto liao! 🙏 ✌️😂`)
		} else if (reports.good / reports.total == 1 && reports.total < 3) {
			this.$el.html(reports.good + ` out of ` + reports.total + `! 100%! Only ` + reports.total + ` reports! But must still celebrate la! 🍺`)
		} else if (reports.good / reports.total > 0.9) {
			this.$el.html(`Wah 90% green! Ho sai bo! 👍👏🤘`)
		} else if (reports.good / reports.total > 0.8) {
			this.$el.html(`Great! 80% green! Today you confirm plus chop on time 👍`)
		} else if (reports.good / reports.total > 0.7) {
			this.$el.html(`Not bad liao... 70% green! You can still make it on time! 👍`)
		} else if (reports.good / reports.total > 0.6) {
			this.$el.html(`Hmmm... Got some line slow hor. So better be careful...`)
		} else if (reports.fault / reports.total > 0.1) {
			this.$el.html(`Careful... Got some train fault hor. Man man lai... 😭`)
		} else if (reports.fault / reports.total > 0.2) {
			this.$el.html(`Wa seh... MRT, why you liddat one?! WHHYYYYYYY! 😭`)
		} else if (reports.slow / reports.total > 0.2) {
			this.$el.html(`Sloooowwww.... More than 20% slow reports ahead... 😒`)
		}
	}
});