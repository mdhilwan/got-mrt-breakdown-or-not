console.log("Got Mrt Breakdown or Not")

window.jQuery = $ 	= require('jquery');
window._			= require('underscore');
window.Popper 		= require('popper.js'),
window.Backbone		= require('backbone'),
window.Marionette	= require('backbone.marionette');

var RootView        = require('./root/views/RootView'),
	Handlebars		= require("hbsfy/runtime"),
    bootstrap       = require('bootstrap');

Handlebars.registerHelper('eq', function(val, val2, block) {
	if(val == val2){
		return block.fn(this);
	}
});

Handlebars.registerHelper('not_eq', function(val, val2, block) {
	if(val != val2){
		return block.fn(this);
	}
});

var BreakdownOrNot = Marionette.Application.extend({
	region: '#root',
	onStart: function () { 
		console.log("app started");
        this.showView(new RootView()); 
	}
})

var breakdownOrNot = new BreakdownOrNot();

breakdownOrNot.start();