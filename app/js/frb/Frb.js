var Mn 			= require('backbone.marionette'),
	firebase    = require('firebase'),
	Radio 		= require('backbone.radio'),
	moment 		= require('moment');

var firebaseRad = Radio.channel('firebaseRad');

var timeToNextReport = 5; // mins

var fireConfig = {
    apiKey: "AIzaSyCY3HSXaqSi7w78t2bTTziSnq0aIsIoZCM",
    authDomain: "gotmrtbreakdownornot.firebaseapp.com",
    databaseURL: "https://gotmrtbreakdownornot.firebaseio.com",
    projectId: "gotmrtbreakdownornot",
    storageBucket: "gotmrtbreakdownornot.appspot.com",
    messagingSenderId: "902174536944"
 };

module.exports = Frb = Mn.Object.extend({

	initialize () {
		firebase.initializeApp(fireConfig);

		this.listenTo(firebaseRad, 'data:getFromPath', this.dataGetFromPath);
		this.listenTo(firebaseRad, 'data:setToPath', this.dataSetToPath);
		this.listenTo(firebaseRad, 'validate:ip', this.validateIp)
	},

	dataGetFromPath (res) {
		var dir = res.path;
		var callback = res.callback;
		var past = moment().subtract(res.reso, 'hour').unix();

		firebase.database().ref(dir + '/').orderByChild('timestamp')
			.startAt(past)
			.endAt(moment().unix())
			.on('value', (db) => {
				$.unblockUI();
				console.warn('data loaded', db.val(), callback);
				firebaseRad.trigger(callback, {
					db: db.val(),
					directory: dir
				});
			})
	},

	dataSetToPath (res) {
		var entry = res.value;
			entry.timestamp = moment().unix();

		firebase.database().ref(res.dir).push(entry);

		if (_.isEmpty(res.hash)) {
			firebase.database().ref('userList/').push(entry)
		} else {
			firebase.database().ref('userList/' + res.hash).set(entry)
		}

		firebaseRad.trigger('data:stored')
	},

	dataDeleteOld () {
		var past = moment().subtract(24, 'hour').unix();
		firebase.database().ref('trainStatus/').orderByChild('timestamp')
			.endAt(past)
			.once('value')
			.then(function (snapshot) {
				console.log('old data:' ,snapshot.val());

				if (_.size(snapshot.val()) > 0) {
					_.each(snapshot.val(), function(reports, hashid) {
						firebase.database().ref('trainStatus/' + hashid).remove();
					});
				}
			})
	},

	validateIp (myIp) {
		firebase.database().ref('userList/').orderByChild('ip').equalTo(myIp.ip).once('value').then(function (snapshot) {
			console.log('validate:ip', snapshot.val())
			if (snapshot.val()) {
				var userIp = snapshot.val();
				_.each(snapshot.val(), function(userIp, hash) {
					var lastPost = moment().diff(moment.unix(userIp.timestamp), 'minutes'); // minutes ago
					console.log('last posted', lastPost);

					if (lastPost >= timeToNextReport) {
						myIp.hash = hash;
						console.log('validated:ip:success', myIp)
						firebaseRad.trigger('validated:ip:success', myIp)		
					} else {
						console.log('validated:ip:fail')
						firebaseRad.trigger('validated:ip:fail', lastPost)		
					}
				});
			} else {
				console.log('validated:ip:success')
				firebaseRad.trigger('validated:ip:success', myIp)
			}
		})
	}
})