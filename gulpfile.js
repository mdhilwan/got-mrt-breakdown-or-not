const fs                = require('fs');
const gulp              = require('gulp');
const util              = require('gulp-util');
const plumber           = require('gulp-plumber');
const size              = require('gulp-size');
const babel             = require('gulp-babel');
const concat            = require('gulp-concat');
const autoprefixer 		= require('gulp-autoprefixer');
const sourcemaps        = require('gulp-sourcemaps');
const gzip              = require('gulp-gzip');
const sass 				= require('gulp-sass');
const gulpif            = require('gulp-if');
const uglify            = require('gulp-uglify-es').default; 
const duration          = require('gulp-duration')
const glob              = require('glob');
const hbsfy             = require("hbsfy");
const babelify          = require('babelify');
const browserify        = require('browserify');
const buffer            = require('vinyl-buffer');
const source            = require('vinyl-source-stream');
const argv              = require('yargs').argv;
const watchify          = require('watchify');
const browserSync       = require('browser-sync').create();
const compress          = require('compression');
const runSequence       = require('run-sequence');
const Spinner           = require('cli-spinner').Spinner;
const packageJson       = require('./package.json')
const SIZE_OPTS         = {
    showFiles: true,
    gzip: true
};

const config = {
	buildFolder: './build'
};

var spinner = new Spinner({
    text: '%s',
    stream: process.stderr,
    onTick: function (msg) {
        this.clearLine(this.stream);
        this.stream.write(msg);
    }
});

watchify.args.debug = true;

hbsfy.configure({
    extensions: ['hbs']
});

const APPS_DIST_DIR = config.buildFolder;
const EXTERNAL_LIBS = packageJson.browser ? [] : packageJson.browser;

var vendor = browserify({
    entries:"./app/js/vendor.js"
});

var bundler = watchify(
    browserify({
        entries:"./app/js/main.js", 
        cache: {},
        packageCache: {},
        debug:true
    })
    .plugin(require('dep-case-verify')));
    //.external(Object.keys(EXTERNAL_LIBS)));

var bundle = () => {
    return bundler
        .transform(hbsfy)
        .bundle()
        .on('error', function (err) {
            console.log('[BROWSERIFY ERROR] ' + err.message);
            util.beep();
            this.emit('end');   
        })
        .on('end', () => {
            util.log(util.colors.bgBlue.bold(' APP BUNDLE COMPLETE! '));
        })
        .pipe(duration('app bundle took:'))
        .pipe(source("app.js"))
        .pipe(buffer())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest(APPS_DIST_DIR));
}

gulp.task("default", () => {
    spinner.start();

    runSequence(
        ["html", "fonts", "styles", "images", "vendor", "bundle"], 
        ["gzip-vendor", "gzip-bundle"],
        "uglify",
        "browserSync"
    );
})

gulp.task("bundle", () => {
    var msg = util.colors.blue('[GULP TASK]') + ' build ' + util.colors.white.bgBlue.bold(' APP ');

    if (argv.prod) {
        util.log(msg + ' lib: building for', util.colors.black.bgWhite.bold(' PRODUCTION '))
    } else if (argv.dev) {
        util.log(msg + ' lib: building for', util.colors.black.bgWhite.bold(' DEV '))
    } else {
        util.log(msg + ' lib: building')
    }

    return bundle();  
})

gulp.task("vendor", () => {
    var msg = util.colors.blue('[GULP TASK]') + ' bundle ' + util.colors.white.bgRed.bold(' VENDOR ');

    if (argv.prod) {
        util.log(msg + ' lib: building for', util.colors.black.bgWhite.bold(' PRODUCTION '))
    } else if (argv.dev) {
        util.log(msg + ' lib: building for', util.colors.black.bgWhite.bold(' DEV '))
    } else {
        util.log(msg + ' lib: building')
    }

    if (EXTERNAL_LIBS) {
	    Object.keys(EXTERNAL_LIBS).forEach((lib) => {
	    	if (lib) vendor.require(lib);
	    });	
    }

    return vendor.bundle()
        .on('error', function (err) {
            console.log('[BROWSERIFY ERROR] ' + err.message);
            util.beep();
            this.emit('end');   
        })
        .on('end', () => {
            util.log(util.colors.bgRed.bold(' VENDOR BUNDLE COMPLETE! '));
        })
        .pipe(duration('vendor bundle took:'))
        .pipe(source("infrastructure.js"))
        .pipe(buffer())
        .pipe(gulp.dest(APPS_DIST_DIR));
})

gulp.task('browserSync', () => {
    if (!argv.prod) {
        util.log(util.colors.black.bgYellow.bold(' STARTING BROWSER-SYNC '));
        browserSync.init({
        	server: {
	            baseDir: config.buildFolder
	        }
        });

        gulp.watch('app/scss/*.scss', ['styles'] );
        gulp.watch("build/**/*.css").on('change', browserSync.reload );
        gulp.watch("app/index.html").on('change', () => {
            html();
            browserSync.reload();
        });
    }

    spinner.stop();
});

var html = () => {
    return gulp.src('./app/index.html')
        .pipe(gulp.dest(config.buildFolder));
}

gulp.task('html', () => {
    return html (); 
});

gulp.task('refresh', () => {
    if (!argv.prod) {
        util.log(util.colors.black.bgWhite.bold(' RELOADING... '));
        browserSync.reload();
    }
});

var errorlog = (err) => {
	console.error(err.message);
	this.emit('end');
}

gulp.task('images', () => {
    gulp.src([
        './node_modules/ion-rangeslider/img/**.*'
        ])
        .pipe(gulp.dest( './'+ config.buildFolder + '/img'));
});

gulp.task('styles', () => {
	gulp.src([
        './node_modules/bootstrap/scss/bootstrap.scss', 
        './node_modules/ion-rangeslider/css/ion.rangeSlider.css', 
        './node_modules/ion-rangeslider/css/ion.rangeSlider.skinFlat.css', 
        './node_modules/font-awesome/scss/font-awesome.scss', 
        'app/scss/style.scss'
        ])
		.pipe(sourcemaps.init())
			.pipe(sass({outputStyle: 'compressed'}))
			.on('error', errorlog)
			.pipe(autoprefixer({
	            browsers: ['last 3 versions'],
	            cascade: false
	        }))	
        .pipe(concat('style.css'))
		.pipe(sourcemaps.write('../maps'))
		.pipe(gulp.dest( './'+ config.buildFolder + '/css'));
});

gulp.task('fonts', () => {
    gulp.src([
        './node_modules/font-awesome/fonts/**.*', 
        ])
        .pipe(gulp.dest( './'+ config.buildFolder + '/fonts'));
});

var gzipBundle = () => {
    if (argv.prod || argv.min) {
        // only happen during --min arguments
        util.log(util.colors.white.bgCyan.bold(' GZIP BUNDLE '));

        return gulp.src('build/app.js')
            .pipe(duration('gzip app.js took:'))
            .pipe(gzip({gzipOptions: {level: 9}}))
            .pipe(gulp.dest(APPS_DIST_DIR));
    } else {
        util.log(util.colors.black.bgWhite.bold(' NO GZIP BUNDLE '), 'run with --min argument to run this function');
    }
}

var gzipVendor = () => {
    if (argv.prod || argv.min) {
        // only happen during --min arguments
        util.log(util.colors.white.bgCyan.bold(' GZIP VENDOR '));

        return gulp.src('build/infrastructure.js')
            .pipe(duration('gzip infrastructure.js took:'))
            .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(sourcemaps.write('./'))
            .pipe(gzip({gzipOptions: {level: 9}}))
            .pipe(gulp.dest(APPS_DIST_DIR))
    } else {
        util.log(util.colors.black.bgWhite.bold(' NO GZIP VENDOR '), 'run with --min argument to run this function');
    }
}

gulp.task('uglify', () => {
    if (argv.prod || argv.min) {
        // only happen during --prod or --min arguments
        util.log(util.colors.black.bgYellow.bold(' UGLIFY + ES6 '));
        return gulp.src('build/app.js')
            .pipe(duration('uglify took:'))
            .pipe(uglify())
            .pipe(gulp.dest(APPS_DIST_DIR))
            .once('end', () => {
                if (argv.prod) {
                    process.exit(0);
                }
            });
    } else {
        util.log(util.colors.black.bgWhite.bold(' NO UGLIFY '), 'run with --min argument to run this function');
    }
})

gulp.task('gzip-vendor', () => {
    return gzipVendor();
})

gulp.task('gzip-bundle', () => {
    return gzipBundle();
})

bundler.on("update", (ids) => {
    var fileChangedConsole = ids.map((file) => {
        return file.substring(file.lastIndexOf('/'), file.length);
    }).join('\n');

    var fileChangedBS = ids.map((file) => {
        return file.substring(file.lastIndexOf('/'), file.length);
    }).join('</br>');

    browserSync.notify("change in the file:</br>" + fileChangedBS);
    util.log(util.colors.bold('change in the file:\n' + fileChangedConsole));

    if (argv.min) {
        return runSequence(
            "bundle", 
            "refresh"
        );
    } else {
        return runSequence(
            "bundle", 
            "refresh"
        );
    }
})